import sys
import time
import hmac, base64, struct, hashlib, time


MAIL_SERVER = 'Server31.areait.lv'
MAIL_USER = 'admin@trollis.lv'
MAIL_PSWRD = '5uXaaDDHSoZm88cK9U'  # 'OX8TudfwoWsy'


def countdown(seconds, message, suffix="Done"):
    disp = f'{message}... {seconds} sec(s) remain'
    disp_len = len(disp)
    for i in range(seconds, 0, -1):
        sys.stdout.write(f'{message}... {i} sec(s) remain'.ljust(disp_len))
        sys.stdout.flush()
        sys.stdout.write('\r')
        sys.stdout.flush()
        time.sleep(1)
    print(f'{message}... {suffix}'.ljust(disp_len))


def get_random_values2(length):
    import random
    dic_str = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm'
    dic_num = '1234567890'
    rnd_str = rnd_num = ''
    for i in range(length):
        rnd_chr = dic_str[random.randint(0, len(dic_str) - 1)]
        rnd_dig = dic_num[random.randint(0, len(dic_num) - 1)]
        rnd_str += rnd_chr
        rnd_num += rnd_dig
    return rnd_str, rnd_num


def check_email_for_token2(delete_mail=True):
    import re
    import poplib
    TOKEN_PATTERN = '=[0-9a-zA-Z=]+'
    timer = 0

    while not timer == 300:
        print('')
        countdown(15, 'Waiting before acessing user mailbox')
        timer += 15  # check mailbox each 15 sec.
        try:
            mailbox = poplib.POP3_SSL(MAIL_SERVER, 995)
            print('Mailbox connected')
        except TimeoutError:
            print('Mail server connection timeout error')
            exit(666)
        mailbox.user(MAIL_USER)
        login = mailbox.pass_(MAIL_PSWRD)
        if 'Logged in' in str(login):
            tot_msg, _ = mailbox.stat()
            for i in range(tot_msg, 0, -1):
                _, msg_line_list, _ = mailbox.retr(i)
                for line in msg_line_list:
                    if 'token' in str(line):
                        token = re.search(TOKEN_PATTERN, str(line)).group(0)
                        if delete_mail:
                            mailbox.dele(i)
                        mailbox.quit()
                        print('Token received by email: {}'.format(token))
                        return token[1:]

        else:
            print('Mail server connection error')
            return None
        mailbox.quit()

    print('No token received in 5 minutes')
    return None


def remove_user_from_DB(user):
    pass #TODO


def get_hotp_token(secret, intervals_no):
    key = base64.b32decode(secret, True)
    msg = struct.pack(">Q", intervals_no)
    h = hmac.new(key, msg, hashlib.sha1).digest()
    o = h[19] & 15
    h = (struct.unpack(">I", h[o:o+4])[0] & 0x7fffffff) % 1000000
    # return h
    return str(h).zfill(6)

def get_totp_token(secret):
    return get_hotp_token(secret, intervals_no=int(time.time())//30)


if __name__ == '__main__':
    check_email_for_token2()
