import os
from gabbi import driver


TESTS_DIR = 'test_development/'


BUILD_TEST_ARGS = {
    'host': 'dev.digitaldealingdesk.eu/user/api/v1',
    'port': 443,
    'test_loader_name': 'apitests',
}


def load_tests(loader, tests, pattern):
    """Provide a TestSuite to the discovery process."""
    test_dir = os.path.join(os.path.dirname(__file__), TESTS_DIR)
    return driver.build_tests(test_dir, loader, **BUILD_TEST_ARGS)
