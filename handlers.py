from functools import partial, reduce
from utils.tools import (check_email_for_token2, get_random_values2,
                         countdown, get_totp_token)


def set_totp_token(test):
    secret = test.test_data.get('tfa_secret')
    token = get_totp_token(secret)
    test.test_data['data']['2fa_password'] = token


def set_random_newuser(test):
    letters, digits = get_random_values2(3)
    first, *rest = letters.lower()
    username = f'{first}{digits}{"".join(rest)}750'  # will be like u489be750
    mail_domain = test.test_data.get('mail_domain', '')
    email = f'{username}@{mail_domain}'
    test.test_data['data']['username'] = username
    test.test_data['data']['email'] = email
    meta = test.test_data.get('meta')
    if not meta:
        test.test_data['meta'] = dict(username=username, email=email)
    else:
        test.test_data['meta'].update(dict(username=username, email=email))
    print(f'\nUser {username} has been created')


def set_email_token(test):
    email_token = test.test_data['meta']['email_token']
    test.test_data['data']['confirm_token'] = email_token


def set_username(test):
    username = test.test_data.get('meta', {}).get('username')
    test.test_data['data']['email_or_username'] = username


def set_auth_token(test):
    tdata = test.test_data
    on_behalf_of = tdata.get('on_behalf_of')
    from_where = tdata['meta'][on_behalf_of] if on_behalf_of else tdata['meta']
    auth_token = from_where.get('auth_token')
    test.test_data['request_headers']['Authorization'] = auth_token


def set_tfa_totp(test):
    tdata = test.test_data
    on_behalf_of = tdata.get('on_behalf_of')
    from_where = tdata['meta'][on_behalf_of] if on_behalf_of else tdata['meta']
    tfa_secret = from_where.get('tfa_secret')
    test.test_data['data']['2fa_password'] = get_totp_token(tfa_secret)


def wait_before_proceed(test):
    # TODO add optional delay settings from yaml
    name = test.test_data['name']
    print('')
    countdown(30, f'Test {name}, preprocess: wait')


def set_email_token_delete(test):
    email_token = test.test_data['meta']['email_token']
    # TODO: here we can simulate errors, e.g. test.test_data.data
    # (wrong dict access)
    test.test_data['data']['delete_confirm_token'] = email_token


def set_email_token_pwd(test):
    email_token = test.test_data['meta']['email_token']
    # TODO: here we can simulate errors, e.g. test.test_data.data
    # (wrong dict access)
    test.test_data['data']['recover_token'] = email_token


def wait_before_proceed_long(test):
    name = test.test_data['name']
    print('')
    countdown(120, f'Test {name}, preprocess: wait')


def set_receiver_wallet(test):
    tdata = test.test_data
    wallet = tdata['meta']['receiver']['blockchain_wallet_address']
    tdata['data']['recipient'] = wallet


def set_trx_amt_curr(test):
    tdata = test.test_data
    amount = tdata['meta']['min_trx_amt']
    currency = tdata['crypto_currency']
    tdata['data']['amount'] = amount
    # round(amount + 0.00003, 8)
    tdata['data']['currency'] = currency


# postprocessor functions
def retrieve_email_token(test):
    email_token = check_email_for_token2()
    if email_token:
        # TODO: decide about verbosity level
        # print(f'Registration token retrieved from mail: {email_token}')
        test.test_data['meta'].update({'email_token': email_token})
    else:
        print('\nDid not receive a registration token')
        test.test_data['meta'].update({'email_token': 'did not receive'})


def save_auth_token(test):
    tdata = test.test_data
    auth_token = test.response_data.get('data', {}).get('auth_token')
    on_behalf_of = tdata.get('on_behalf_of')
    entry_to_upd = tdata['meta'][on_behalf_of] if on_behalf_of else tdata['meta']
    entry_to_upd.update({'auth_token': auth_token})
    # print(f'User auth token: {auth_token}')


def save_tfa_secret(test):
    tfa_secret = test.response_data.get('data', {}
                                        ).get('settings', {}
                                              ).get('2fa_secret_key')
    # print(f'User tfa secret: {tfa_secret}')
    test.test_data['meta'].update({'tfa_secret': tfa_secret})


def save_user_balance(test):
    tdata = test.test_data
    on_behalf_of = tdata.get('on_behalf_of')
    entry_to_upd = tdata['meta'][on_behalf_of] if on_behalf_of else tdata['meta']
    blockchain_wallet_address = entry_to_upd['blockchain_wallet_address']
    wallet = filter(lambda x: x['blockchain_wallet_address'] == blockchain_wallet_address,
                    test.response_data['data']['wallets']).__next__()
    balance_before = wallet['amount'] - wallet['reserved_amount']
    entry_to_upd.update({'balance_before': balance_before})


def save_min_trx_amt(test):
    def keylookup(key, item):
        return item['setting_type'] == key

    tdata = test.test_data
    path_limits = test.response_data['data']['transaction_limit_settings']
    path_fees = test.response_data['data']['transaction_fee_settings']
    min_lookup = partial(
        keylookup,
        (f"{tdata['trx_type']} Transaction {tdata['crypto_currency']}"
         f" MIN amount")
    )
    fee_loookup = partial(
        keylookup,
        f"{tdata['trx_type']} Transaction fee"
    )
    min_ = filter(min_lookup, path_limits).__next__()
    fee = filter(fee_loookup, path_fees).__next__()
    tdata['meta'].update({
        'min_trx_amt': min_['setting_value_decimal'],
        'trx_fee': fee['setting_value_decimal'],
    })


def save_min_trx_amt_withdr(test):
    def minlookup(key, item):
        return item['setting_type'] == key

    def feelookup(percent_fee, abs_fee, acc, x):
        key = x['setting_type']
        value = x['setting_value_decimal']
        if key == percent_fee:
            acc['percent_fee'] = value
        if key == abs_fee:
            acc['abs_fee'] = value
        return acc

    tdata = test.test_data
    path_limits = test.response_data['data']['transaction_limit_settings']
    path_fees = test.response_data['data']['transaction_fee_settings']
    min_lookup = partial(
        minlookup,
        (f"{tdata['trx_type']} Transaction {tdata['crypto_currency']}"
         f" MIN amount")
    )
    fee_lookup = partial(
        feelookup,
        f"{tdata['trx_type']} Transaction fee",
        f"{tdata['crypto_currency']} Blockchain {tdata['trx_type'].lower()} fee"
    )
    min_ = filter(min_lookup, path_limits).__next__()
    fees = reduce(fee_lookup, path_fees, {})
    tdata['meta'].update({'min_trx_amt': min_['setting_value_decimal']})
    tdata['meta'].update(fees)


def compare_balance(test):
    calc_diff = {
        'sender': lambda amt, fee: -amt * (1 + fee),
        'receiver': lambda amt, fee: amt,
    }
    tdata = test.test_data
    on_behalf_of = tdata['on_behalf_of']
    blockchain_wallet_address = tdata['meta'][on_behalf_of]['blockchain_wallet_address']
    amount = tdata['meta']['min_trx_amt']
    fee = tdata['meta']['trx_fee']
    round_factor = tdata['round_factor']
    balance_before = tdata['meta'][on_behalf_of]['balance_before']
    wallet = filter(lambda x: x['blockchain_wallet_address'] == blockchain_wallet_address,
                    test.response_data['data']['wallets']).__next__()
    balance_after = wallet['amount'] - wallet['reserved_amount']
    expected = calc_diff[on_behalf_of](amount, fee)
    calculated = balance_after - balance_before
    # TODO: assertion errors here should do failed test instead of prog error
    assert(round(calculated, round_factor) == round(expected, round_factor))


def compare_balance_withdr(test):
    tdata = test.test_data
    on_behalf_of = tdata['on_behalf_of']
    blockchain_wallet_address = tdata['meta'][on_behalf_of]['blockchain_wallet_address']
    amount = tdata['meta']['min_trx_amt']
    percent_fee = tdata['meta']['percent_fee']
    abs_fee = tdata['meta']['abs_fee']
    round_factor = tdata['round_factor']
    balance_before = tdata['meta'][on_behalf_of]['balance_before']
    wallet = filter(lambda x: x['blockchain_wallet_address'] == blockchain_wallet_address,
                    test.response_data['data']['wallets']).__next__()
    balance_after = wallet['amount'] - wallet['reserved_amount']
    expected = -(amount * (1 + percent_fee) + abs_fee)
    calculated = balance_after - balance_before
    # TODO: assertion errors here should do failed test instead of prog error
    assert(round(calculated, round_factor) == round(expected, round_factor))


def test_last_trans_amounts(test):
    tdata = test.test_data
    on_behalf_of = tdata.get('on_behalf_of')
    from_where = tdata['meta'][on_behalf_of] if on_behalf_of else tdata['meta']
    # TODO: maybe to avoid history['exact test name'] and do search test with 'amount' key?
    expected_amount = test.history['do transaction'].test_data['data']['amount']
    # TODO: maybe programmatical sorting here?
    actual_amount = test.response_data['data']['transactions'][0]['amount']
    test.assertEqual(actual_amount, expected_amount)


PREPROCESS = {
    'set_tfa_token': set_totp_token,
    'make_and_set_user': set_random_newuser,
    'set_email_token': set_email_token,
    'set_username': set_username,
    'set_auth_token': set_auth_token,
    'set_tfa_totp': set_tfa_totp,
    'wait_before_proceed': wait_before_proceed,
    'set_email_token_delete': set_email_token_delete,
    'set_email_token_pwd': set_email_token_pwd,
    'wait_before_proceed_long': wait_before_proceed_long,
    'set_receiver_wallet': set_receiver_wallet,
    'set_trx_amt_curr': set_trx_amt_curr,
}


POSTPROCESS = {
    'retrieve_email_token': retrieve_email_token,
    'save_auth_token': save_auth_token,
    'save_tfa_secret': save_tfa_secret,
    'save_user_balance': save_user_balance,
    'save_min_trx_amt': save_min_trx_amt,
    'save_min_trx_amt_withdr': save_min_trx_amt_withdr,
    'compare_balance': compare_balance,
    'compare_balance_withdr': compare_balance_withdr,
    'test_last_trans_amounts': test_last_trans_amounts,
}


def invoke_handlers(test, pre_or_postprocess):
    func_table = {
        'preprocess': PREPROCESS,
        'postprocess': POSTPROCESS,
    }
    handlers = test.test_data.get(pre_or_postprocess, [])
    actions = handlers if isinstance(handlers, list) else [handlers]
    for action in actions:
        func_table[pre_or_postprocess][action](test)
